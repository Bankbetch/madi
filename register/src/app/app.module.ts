import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { MathRandomComponent } from './math-random/math-random.component';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { TableComponent } from './register/table/table.component'
import { Routes, RouterModule } from '@angular/router'

const appRoutes: Routes = [
  { path: 'list', component: TableComponent },
  { path: '', component: RegisterComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    MathRandomComponent,
    RegisterComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
