import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math-random',
  templateUrl: './math-random.component.html',
  styleUrls: ['./math-random.component.css']
})
export class MathRandomComponent {

  mathA = Math.floor((Math.random() * 100) + 1);
  math = 0;
  title = ""
  test = "hello"
  round = 0;

  OnclickCheck() {
    if (this.mathA < this.math) {
      this.title = "To hight"
      this.round += 1;
    }

    if (this.mathA > this.math) {
      this.title = "To Low"
      this.round += 1;
    }
    if (this.mathA == this.math) {
      this.title = "Correct"
      this.round += 1;
    }

  }
  Random() {
    this.mathA = Math.floor((Math.random() * 100) + 1);
    this.title = ""
    this.round = 0
  }

  isNumberKey(event) {
    var keycode = event.keyCode;
    if (keycode >= 48 && keycode <= 57) {
      return true
    }
    return false
  }



  onEnter(event) {
    if (event.key == "Enter") {
      this.action()
      this.math = 0
    }
  }

  action() {
    if (this.mathA < this.math) {
      this.title = "To hight"
      this.round += 1;
    }

    if (this.mathA > this.math) {
      this.title = "To Low"
      this.round += 1;
    }
    if (this.mathA == this.math) {
      this.title = "Correct"
      this.round += 1;
    }
  }
}
