var mongoose = require('mongoose')

//เชื่อมต่อ mongodb


mongoose.connect('mongodb://@ds127995.mlab.com:27995/madi', {
  useNewUrlParser: true,
  auth: {
    user: 'admin',
    password: 'a123456'
  }
})

mongoose.connection.on('connected',function(){
    console.log('Mongoose defualt connect open')
})

mongoose.connection.on('error',function(err){
    console.log('Mongoose defualt connection error: ' + err)
})

mongoose.connection.on('disconnected',function(){
    console.log('Mongoose defualt connect disconnected')
})

process.on('SIGNT',function(){
    mongoose.connect.close(function(){
        console.log('mongoose defualt connection disconnected through app termination')
        
    })
})