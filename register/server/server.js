const express = require('express')
const app = express()
const bodyParser = require('body-parser')
require('./db')
const registerModel = require('./register')
const educationModel = require('./education')

app.use(bodyParser.json())


app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

app.get('/list', (req, res) => {
    registerModel.find((err, doc) => {
        res.json({ data: doc })
    })
})

app.get('/list/:id', (req, res) => {
    registerModel.findById(req.params.id, (err, doc) => {
        res.json({ data: doc })
    })
})


app.get('/', (req, res, next) => {
    educationModel.find((err, doc) => {
        res.json({ data: doc })
    })
})

app.post('/', (req, res) => {
    const username = req.body.username
    const Emaill = req.body.Email
    const Password = req.body.Password
    const firstName = req.body.firstName
    const lastName = req.body.lastName
    const education = req.body.education
    const country = req.body.countrycheck
    const sex = req.body.sex

    registerModel.create(req.body, (err, doc) => {
        // doc คือผลลัพธ์ที่ได้จากการ insert
        if (err) {
            res.json(err)
        }
        
    })
})


app.delete('/list/:id', (req, res) => {
    registerModel.deleteOne({ _id: req.params.id }, (err, doc) => {
        res.json({ data: doc })
    })
})
app.put('/list/:id', (req, res) => {

    const post ={
        _id: req.params.id,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        education: req.body.education,
        country: req.body.country,
        sex: req.body.sex
    }
    registerModel.updateOne({ _id: req.params.id }, post,(err,doc)=>{
        res.json({ data: doc })
    })
})

app.listen(3000, () => {
    console.log('server running')
})