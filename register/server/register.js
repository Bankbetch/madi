var mongoose = require('mongoose')


var registerSchema = mongoose.Schema({
    username: String,//บังคับว่า type ต้องเป็น string และต้องมีข้อมูลส่งมา
    email: String,
    password: String,
    firstname: String,
    lastname: String,
    education:Array,
    country:String,
    sex:String
})

var registerModel = mongoose.model('registers', registerSchema)

module.exports = registerModel