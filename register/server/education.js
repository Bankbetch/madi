var mongoose = require('mongoose')


var educationSchema = mongoose.Schema({
   id:"string",
   education:"string"
})

var educationModel = mongoose.model('educations', educationSchema)

module.exports = educationModel